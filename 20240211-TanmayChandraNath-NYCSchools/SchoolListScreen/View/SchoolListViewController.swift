//
//  SchoolListViewController.swift
//  20240211-TanmayChandraNath-NYCSchools
//
//  Created by Tanmay Chandra Nath on 11/02/24.
//

import UIKit

class SchoolListViewController: UIViewController {
    
    // MARK: IBOutles
    @IBOutlet private weak var loaderView: UIActivityIndicatorView!
    @IBOutlet private weak var tableView: UITableView!
    // MARK: Variables
    
    var viewModel: SchoolListViewModel!
    var networkCaller = SchoolListNetworkCaller()
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCells()
        tableView.dataSource = self
        tableView.delegate = self
        viewModel = SchoolListViewModel(delegate: self, callerDelegate: networkCaller)
        viewModel.fetchDetails()
        self.navigationItem.title = ScreenNames.schoolList.rawValue
    }
    
    private func registerCells() {
        tableView.register(UINib(nibName: "SchoolListTableViewCell", bundle: nil), forCellReuseIdentifier: "SchoolListTableViewCell")
    }
}

// MARK: Loader view helper methods

extension SchoolListViewController {
    
    func showLoader() {
        loaderView.isHidden = false
        loaderView.startAnimating()
    }
    
    func hideLoader() {
        loaderView.isHidden = true
        loaderView.stopAnimating()
    }
}

// MARK: Tableview data source methods

extension SchoolListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.schoolAndSatScoreList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolListTableViewCell", for: indexPath) as? SchoolListTableViewCell else { return UITableViewCell() }
        cell.configure(data: SchoolListDataToShow(schoolName: viewModel.schoolAndSatScoreList[indexPath.row].schoolName, borough: viewModel.schoolAndSatScoreList[indexPath.row].borough, city: viewModel.schoolAndSatScoreList[indexPath.row].city))
        return cell
    }
}

// MARK: Table view delegate methods
extension SchoolListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let schoolDetailVc = SchoolDetailViewController(nibName: "SchoolDetailViewController", bundle: nil)
        schoolDetailVc.details = viewModel.schoolAndSatScoreList[indexPath.row]
        self.navigationController?.pushViewController(schoolDetailVc, animated: true)
    }
}

// MARK: conforming SchoolListScreenProtocol methods

extension SchoolListViewController: SchoolListScreenProtocol {
    func startedNetworkCall() {
        DispatchQueue.main.async {
            self.showLoader()
        }
    }
    
    func finishedNetworkCall() {
        DispatchQueue.main.async {
            self.hideLoader()
        }
    }
    
    func showData() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func showError() {
        self.showCustomAlert(header: StringConstants.somethingWentWrong, subheader: StringConstants.pleaseTryAgain, okButtonTitle: StringConstants.retry, cancelButtonTitle: StringConstants.cancel) {
            self.viewModel.fetchDetails()
        } cancelButtonAction: {
            self.navigationController?.popViewController(animated: true)
        }
    }
}

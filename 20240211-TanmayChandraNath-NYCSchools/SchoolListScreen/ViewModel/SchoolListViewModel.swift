//
//  SchoolListViewModel.swift
//  20240211-TanmayChandraNath-NYCSchools
//
//  Created by Tanmay Chandra Nath on 11/02/24.
//

import Foundation

protocol SchoolListScreenProtocol: AnyObject {
    func startedNetworkCall()
    func finishedNetworkCall()
    func showData()
    func showError()
}

protocol SchoolListCallerProtocol: AnyObject {
    func getSchoolListData(completionHandler: @escaping ((Result<[SchoolDetailModel], NetworkError>) -> Void))
    func getSatScoreListData(completionHandler: @escaping ((Result<[SatScoreModel], NetworkError>) -> Void))
}

class SchoolListViewModel {
    private var schoolListData: [SchoolDetailModel] = []
    private var schoolListError: NetworkError?
    private var satScoreListData: [SatScoreModel] = []
    private var satScoreListError: NetworkError?
    var schoolAndSatScoreList: [SchoolAndSatScoreModel] = []
    var errorToShow: NetworkError?
    
    weak var delegate: SchoolListScreenProtocol?
    weak var callerDelegate: SchoolListCallerProtocol?
    
    init(delegate: SchoolListScreenProtocol? = nil, callerDelegate: SchoolListCallerProtocol? = nil) {
        self.delegate = delegate
        self.callerDelegate = callerDelegate
    }
    
    func fetchDetails() {
        delegate?.startedNetworkCall()
        fetchSchoolList()
        fetchSatScores()
    }
    
    private func fetchSatScores() {
        satScoreListCallCompleted = false
        callerDelegate?.getSatScoreListData() {[weak self] (result: Result<[SatScoreModel], NetworkError>) in
            guard let currentSelf = self else { return }
            switch result {
            case .success(let data):
                currentSelf.satScoreListData = data
                currentSelf.satScoreListError = nil
            case .failure(let error):
                currentSelf.satScoreListError = error
            }
            currentSelf.satScoreListCallCompleted = true

        }
    }
    
    private func fetchSchoolList() {
        schoolListCallCompleted = false
        callerDelegate?.getSchoolListData() {[weak self] (result: Result<[SchoolDetailModel], NetworkError>) in
            guard let currentSelf = self else { return }
            switch result {
            case .success(let data):
                
                currentSelf.schoolListData = data
                currentSelf.schoolListError = nil
            case .failure(let error):
                currentSelf.schoolListError = error
            }
            currentSelf.schoolListCallCompleted = true
        }
    }
    
    private func constructCombinedDetails() -> [SchoolAndSatScoreModel] {
        var finalList: [SchoolAndSatScoreModel] = []
        for schoolData in schoolListData {
            for satScoreData in satScoreListData where schoolData.dbn == satScoreData.dbn {
                let newData = SchoolAndSatScoreModel(schoolName: schoolData.schoolName, schoolOverview: schoolData.schoolOverview, academicOpportunitiesOne: schoolData.academicOpportunitiesOne, academicOpportunitiesTwo: schoolData.academicOpportunitiesOne, borough: schoolData.borough, city: schoolData.city, dbn: schoolData.dbn, satCriticalReadingAvgScore: satScoreData.satCriticalReadingAvgScore, satMathAvgScore: satScoreData.satMathAvgScore, satWritingAvgScore: satScoreData.satWritingAvgScore, extraCurricularActivities: schoolData.extraCurricularActivities)
                
                finalList.append(newData)
            }
        }
        return finalList
    }
    
    private var allApiCallCompleted: Bool = false {
        didSet {
            if allApiCallCompleted {
                delegate?.finishedNetworkCall()
                if schoolListError == nil && satScoreListError == nil {
                    schoolAndSatScoreList = constructCombinedDetails()
                    delegate?.showData()
                } else {
                    errorToShow = schoolListError ?? satScoreListError
                    delegate?.showError()
                }
            }
        }
    }
    
    private var satScoreListCallCompleted: Bool = false {
        didSet {
            allApiCallCompleted = schoolListCallCompleted && satScoreListCallCompleted
        }
    }
    
    private var schoolListCallCompleted: Bool = false {
        didSet {
            allApiCallCompleted = schoolListCallCompleted && satScoreListCallCompleted
        }
    }
}

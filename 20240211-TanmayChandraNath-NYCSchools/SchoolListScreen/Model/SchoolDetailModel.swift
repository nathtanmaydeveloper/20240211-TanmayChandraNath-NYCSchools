//
//  SchoolDetailModel.swift
//  20240211-TanmayChandraNath-NYCSchools
//
//  Created by Tanmay Chandra Nath on 11/02/24.
//

import Foundation

struct SchoolDetailModel: Decodable {
    let schoolName, schoolOverview, academicOpportunitiesOne, academicOpportunitiesTwo, borough, city, dbn: String?
    let extraCurricularActivities: String?
    
    enum CodingKeys: String, CodingKey {
        case schoolName = "school_name"
        case schoolOverview = "overview_paragraph"
        case academicOpportunitiesOne = "academicopportunities1"
        case academicOpportunitiesTwo = "academicopportunities2"
        case borough
        case city
        case dbn
        case extraCurricularActivities = "extracurricular_activities"
    }
}

struct SatScoreModel: Decodable {
    let satCriticalReadingAvgScore, satMathAvgScore, satWritingAvgScore, dbn: String?
    
    enum CodingKeys: String, CodingKey {
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satWritingAvgScore = "sat_writing_avg_score"
        case dbn
    }
}

struct SchoolAndSatScoreModel {
    let schoolName, schoolOverview, academicOpportunitiesOne, academicOpportunitiesTwo, borough, city, dbn, satCriticalReadingAvgScore, satMathAvgScore, satWritingAvgScore, extraCurricularActivities: String?
}

//
//  SchoolListTableViewCell.swift
//  20240211-TanmayChandraNath-NYCSchools
//
//  Created by Tanmay Chandra Nath on 11/02/24.
//

import UIKit

class SchoolListTableViewCell: UITableViewCell {

    @IBOutlet private weak var locationLabel: UILabel!
    @IBOutlet private weak var nameLabel: UILabel!
    
    func configure(data: SchoolListDataToShow) {
        locationLabel.text = data.location
        nameLabel.text = data.schoolName
    }
}

struct SchoolListDataToShow {
    let schoolName: String?
    var borough: String?
    let city: String?
    
    var location: String? {
        var loc = ""
        if borough != nil && !borough!.isEmpty {
            loc.append(borough!.trimmingCharacters(in: .whitespaces))
            loc.append(", ")
        }
        loc.append(city?.trimmingCharacters(in: .whitespaces) ?? "")
        return loc
    }
}


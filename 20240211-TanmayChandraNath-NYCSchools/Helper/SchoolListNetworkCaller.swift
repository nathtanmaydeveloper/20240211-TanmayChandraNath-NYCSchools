//
//  SchoolListNetworkCaller.swift
//  20240211-TanmayChandraNath-NYCSchools
//
//  Created by Tanmay Chandra Nath on 11/02/24.
//

import Foundation

class SchoolListNetworkCaller: SchoolListCallerProtocol {
    func getSchoolListData(completionHandler: @escaping ((Result<[SchoolDetailModel], NetworkError>) -> Void)) {
        NetworkServiceManager.shared.getData(urlString: ApiUrls.schoolListUrl.rawValue, completionHandler: completionHandler)
    }
    
    func getSatScoreListData(completionHandler: @escaping ((Result<[SatScoreModel], NetworkError>) -> Void)) {
        NetworkServiceManager.shared.getData(urlString: ApiUrls.satScoreListUrl.rawValue, completionHandler: completionHandler)
    }
    
    
}

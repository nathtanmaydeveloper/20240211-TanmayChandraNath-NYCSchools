//
//  NetworkServiceManager.swift
//  20240211-TanmayChandraNath-NYCSchools
//
//  Created by Tanmay Chandra Nath on 11/02/24.
//

import Foundation

enum NetworkError: Error {
    case invalidUrl
    case invalidData
    case invalidResponse
    case otherError(_ error: Error)
}

class NetworkServiceManager {
    static let shared = NetworkServiceManager()
    private init() {}
    
    
    func getData<T: Decodable>(urlString: String, completionHandler: @escaping ((Result<T, NetworkError>) -> Void)) {
        guard let url = URL(string: urlString) else {
            completionHandler(.failure(.invalidUrl))
            return
        }
        let urlRequest = URLRequest(url: url)
        
        configuredUrlSession.dataTask(with: urlRequest) { data, response, error in
            /// Checking for all error scenarios
            guard let data = data else {
                completionHandler(.failure(.invalidData))
                return
            }
            guard let httpUrlResponse = (response as? HTTPURLResponse) else {
                completionHandler(.failure(.invalidResponse))
                return
            }
            if (httpUrlResponse.statusCode != 200) {
                completionHandler(.failure(.invalidResponse))
                return
            }
            if let error = error {
                completionHandler(.failure(.otherError(error)))
                return
            }
            /// Decoding data if no error is there
            do {
                let result = try JSONDecoder().decode(T.self, from: data)
                completionHandler(.success(result))
            } catch (let error) {
                completionHandler(.failure(.otherError(error)))
            }
            
        }.resume()
    }
}

extension NetworkServiceManager {
    var configuredUrlSession: URLSession {
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = NetworkCallTImeouts.requestTimeOutTime.rawValue
        sessionConfig.timeoutIntervalForResource = NetworkCallTImeouts.resourceTimeOutTime.rawValue
        return URLSession(configuration: sessionConfig)
    }
}

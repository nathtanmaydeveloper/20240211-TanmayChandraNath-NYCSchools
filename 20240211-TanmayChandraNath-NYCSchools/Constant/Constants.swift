//
//  Constants.swift
//  20240211-TanmayChandraNath-NYCSchools
//
//  Created by Tanmay Chandra Nath on 11/02/24.
//

import Foundation

enum ApiUrls: String {
    case schoolListUrl = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    case satScoreListUrl = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
}

enum SatScoreHeaders: String {
    case math = "Math SAT Score"
    case writing = "Writing SAT Score"
    case reading = "Reading SAT Score"
}

enum SchoolDetailsSectionNames: String {
    case topOpportuinity = "Top Opportunity"
    case extraCurricularActivities = "Extra Curricular Activities"
}

enum NetworkCallTImeouts: TimeInterval {
    case requestTimeOutTime = 30
    case resourceTimeOutTime = 60
}

enum ScreenNames: String {
    case schoolList = "List of Schools"
    case schoolDetail = "School Details"
}

struct StringConstants {
    static let noDetailsFound = "No Details Found"
    static let notFound = "Not Found"
    static let somethingWentWrong = "Something went wrong!"
    static let pleaseTryAgain = "Please Try Again"
    static let retry = "Retry"
    static let cancel = "Cancel"
}


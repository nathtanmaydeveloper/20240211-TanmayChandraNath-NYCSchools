//
//  UIViewController+Extension.swift
//  20240211-TanmayChandraNath-NYCSchools
//
//  Created by Tanmay Chandra Nath on 11/02/24.
//

import Foundation
import UIKit

typealias VoidFunction = (() -> Void)

extension UIViewController {
    func showCustomAlert(header: String?, subheader: String?, okButtonTitle: String?, cancelButtonTitle: String?, okButtonAction: VoidFunction?, cancelButtonAction: VoidFunction?) {
        let alertController = UIAlertController(title: header, message: subheader, preferredStyle: .alert)
        
        var okAction: UIAlertAction?
        if let okButtonTitle = okButtonTitle{
            okAction = UIAlertAction(title: okButtonTitle, style: .default) { (action:UIAlertAction!) in
                if let okButtonAction = okButtonAction {
                    okButtonAction()
                }
            }
        }
        
        var cancelAction: UIAlertAction?
        if let cancelButtonTitle = cancelButtonTitle{
            cancelAction = UIAlertAction(title: cancelButtonTitle, style: .default) { (action:UIAlertAction!) in
                if let cancelButtonAction = cancelButtonAction {
                    cancelButtonAction()
                }
            }
        }
        
        if let cancelAction = cancelAction {
            alertController.addAction(cancelAction)
        }
        
        if let okAction = okAction {
            alertController.addAction(okAction)
        }
        
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion:nil)
        }
    }
}

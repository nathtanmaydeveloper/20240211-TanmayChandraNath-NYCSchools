//
//  SchoolDetailDataSource.swift
//  20240211-TanmayChandraNath-NYCSchools
//
//  Created by Tanmay Chandra Nath on 12/02/24.
//

import Foundation
import UIKit

protocol SchoolDetailDataSourceDelegate: AnyObject {
    var schoolDetails: SchoolAndSatScoreModel? {get}
}

class SchoolDetailDataSource: NSObject, UITableViewDataSource {
    weak var delegate: SchoolDetailDataSourceDelegate?
    
    init(delegate: SchoolDetailDataSourceDelegate? = nil) {
        super.init()
        self.delegate = delegate
    }
    
    enum SchoolDetailCellTypes: Int, CaseIterable {
        case schoolHeader
        case mathSatScore
        case writingSatScore
        case readingSatScore
        case topOpportunity
        case extraCurricularActivity
    }
    
    // MARK: Tableview data source methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        SchoolDetailCellTypes.allCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case SchoolDetailCellTypes.schoolHeader.rawValue : return getSchoolHeaderCell(tableView, indexPath)
        case SchoolDetailCellTypes.extraCurricularActivity.rawValue : return getExtraCurricularDetailsCell(tableView, indexPath)
        case SchoolDetailCellTypes.topOpportunity.rawValue : return getOpportuntiyDetailsCell(tableView, indexPath)
        case SchoolDetailCellTypes.mathSatScore.rawValue : return getMathSatScoreCell(tableView, indexPath)
        case SchoolDetailCellTypes.writingSatScore.rawValue : return getWritingSatScoreCell(tableView, indexPath)
        case SchoolDetailCellTypes.readingSatScore.rawValue : return getReadingSatScoreCell(tableView, indexPath)
        default: return UITableViewCell()
        }
    }
}

// MARK: To get different type of table view cells

extension SchoolDetailDataSource {
    private func getExtraCurricularDetailsCell(_ tableView: UITableView, _ indexPath: IndexPath)  -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TitleWithDetailsTableViewCell", for: indexPath) as? TitleWithDetailsTableViewCell else {return UITableViewCell()}
        cell.configure(title: SchoolDetailsSectionNames.extraCurricularActivities.rawValue, details: delegate?.schoolDetails?.extraCurricularActivities)
        return cell
    }
    
    private func getOpportuntiyDetailsCell(_ tableView: UITableView, _ indexPath: IndexPath)  -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TitleWithDetailsTableViewCell", for: indexPath) as? TitleWithDetailsTableViewCell else {return UITableViewCell()}
        cell.configure(title: SchoolDetailsSectionNames.topOpportuinity.rawValue, details: delegate?.schoolDetails?.academicOpportunitiesOne)
        return cell
    }
    
    private func getSchoolHeaderCell(_ tableView: UITableView, _ indexPath: IndexPath)  -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolHeaderTableViewCell", for: indexPath) as? SchoolHeaderTableViewCell else {return UITableViewCell()}
        cell.configure(header: delegate?.schoolDetails?.schoolName, description: delegate?.schoolDetails?.schoolOverview)
        return cell
    }
    
    private func getMathSatScoreCell(_ tableView: UITableView,_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SatScoreTableViewCell", for: indexPath) as? SatScoreTableViewCell else {return UITableViewCell()}
        cell.configure(title: SatScoreHeaders.math.rawValue, satScore: delegate?.schoolDetails?.satMathAvgScore)
        return cell
    }
    
    private func getReadingSatScoreCell(_ tableView: UITableView, _ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SatScoreTableViewCell", for: indexPath) as? SatScoreTableViewCell else {return UITableViewCell()}
        cell.configure(title: SatScoreHeaders.reading.rawValue, satScore: delegate?.schoolDetails?.satCriticalReadingAvgScore)
        return cell
    }
    
    private func getWritingSatScoreCell(_ tableView: UITableView, _ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SatScoreTableViewCell", for: indexPath) as? SatScoreTableViewCell else {return UITableViewCell()}
        cell.configure(title: SatScoreHeaders.writing.rawValue, satScore: delegate?.schoolDetails?.satWritingAvgScore)
        return cell
    }
}

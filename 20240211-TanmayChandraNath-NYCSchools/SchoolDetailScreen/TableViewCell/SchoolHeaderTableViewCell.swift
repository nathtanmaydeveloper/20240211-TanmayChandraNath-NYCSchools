//
//  SchoolHeaderTableViewCell.swift
//  20240211-TanmayChandraNath-NYCSchools
//
//  Created by Tanmay Chandra Nath on 11/02/24.
//

import UIKit

class SchoolHeaderTableViewCell: UITableViewCell {

    
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var headerLabel: UILabel!
 
    func configure(header: String?, description: String?) {
        headerLabel.text = header
        descriptionLabel.text = description
    }
}

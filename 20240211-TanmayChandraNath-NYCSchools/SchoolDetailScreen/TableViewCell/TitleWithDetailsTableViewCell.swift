//
//  TitleWithDetailsTableViewCell.swift
//  20240211-TanmayChandraNath-NYCSchools
//
//  Created by Tanmay Chandra Nath on 11/02/24.
//

import UIKit

class TitleWithDetailsTableViewCell: UITableViewCell {

    @IBOutlet private weak var detailsLabel: UILabel!
    @IBOutlet private weak var titleLabel: UILabel!
    
    func configure(title: String?, details: String?) {
        titleLabel.text = title
        detailsLabel.text = details ?? StringConstants.noDetailsFound
    }
}

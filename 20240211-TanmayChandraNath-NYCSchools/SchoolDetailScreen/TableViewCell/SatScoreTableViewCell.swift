//
//  SatScoreTableViewCell.swift
//  20240211-TanmayChandraNath-NYCSchools
//
//  Created by Tanmay Chandra Nath on 11/02/24.
//

import UIKit

class SatScoreTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var satScoreLabel: UILabel!
    
    func configure(title: String?, satScore: String?) {
        titleLabel.text = title
        satScoreLabel.text = satScore ?? StringConstants.notFound
    }
}

//
//  SchoolDetailViewController.swift
//  20240211-TanmayChandraNath-NYCSchools
//
//  Created by Tanmay Chandra Nath on 11/02/24.
//

import UIKit

class SchoolDetailViewController: UIViewController {

    // MARK: IBOutlets
    @IBOutlet private weak var tableView: UITableView!
    // MARK: Variables
    var details: SchoolAndSatScoreModel?
    var datasource: SchoolDetailDataSource!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datasource = SchoolDetailDataSource(delegate: self)
        registerCells()
        tableView.dataSource = datasource
        self.navigationItem.title = ScreenNames.schoolDetail.rawValue
    }
    
    private func registerCells() {
        tableView.register(UINib(nibName: "SatScoreTableViewCell", bundle: nil), forCellReuseIdentifier: "SatScoreTableViewCell")
        tableView.register(UINib(nibName: "SchoolHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "SchoolHeaderTableViewCell")
        tableView.register(UINib(nibName: "TitleWithDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "TitleWithDetailsTableViewCell")
    }
}

// MARK: SchoolDetailDataSourceDelegate methods
extension SchoolDetailViewController : SchoolDetailDataSourceDelegate {
    var schoolDetails: SchoolAndSatScoreModel? {
        return details
    }
}

//
//  ViewController.swift
//  20240211-TanmayChandraNath-NYCSchools
//
//  Created by Tanmay Chandra Nath on 11/02/24.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var proceedButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func proceedButtonClicked(_ sender: UIButton) {
        let nextVc = SchoolListViewController(nibName: "SchoolListViewController", bundle: nil)
        self.navigationController?.pushViewController(nextVc, animated: true)
    }
    
}


//
//  SchoolListFileReader.swift
//  20240211-TanmayChandraNath-NYCSchoolsTests
//
//  Created by Tanmay Chandra Nath on 11/02/24.
//

import Foundation
@testable import _0240211_TanmayChandraNath_NYCSchools

class SchoolListFileReader: SchoolListCallerProtocol {
    var didSchoolListApiCalled = false
    var didSatScoreListApiCalled = false
    var didBothApiCalled : Bool {
        didSchoolListApiCalled && didSatScoreListApiCalled
    }
    
    func getSchoolListData(completionHandler: @escaping ((Result<[_0240211_TanmayChandraNath_NYCSchools.SchoolDetailModel], _0240211_TanmayChandraNath_NYCSchools.NetworkError>) -> Void)) {
        didSchoolListApiCalled = true
        FileReaderServiceManager.shared.getData(fileName: "SchoolListDummyData", completionHandler: completionHandler)
    }
    
    func getSatScoreListData(completionHandler: @escaping ((Result<[_0240211_TanmayChandraNath_NYCSchools.SatScoreModel], _0240211_TanmayChandraNath_NYCSchools.NetworkError>) -> Void)) {
        didSatScoreListApiCalled = true
        FileReaderServiceManager.shared.getData(fileName: "SatScoreListDummyData", completionHandler: completionHandler)
    }
    
    
}

//
//  FileReaderServiceManager.swift
//  20240211-TanmayChandraNath-NYCSchoolsTests
//
//  Created by Tanmay Chandra Nath on 11/02/24.
//

import Foundation
@testable import _0240211_TanmayChandraNath_NYCSchools

class FileReaderServiceManager {
    static let shared = FileReaderServiceManager()
    private init() {}
    
    func getData<T: Decodable>(fileName: String, completionHandler: @escaping ((Result<T, NetworkError>) -> Void)) {
        guard let url = Bundle.main.url(forResource: fileName, withExtension: "json") else {
            completionHandler(.failure(.invalidUrl))
            return
        }
        guard let data = try? Data(contentsOf: url) else {
            completionHandler(.failure(.invalidData))
            return
        }
        do {
            let result = try JSONDecoder().decode(T.self, from: data)
            completionHandler(.success(result))
        } catch (let error) {
            completionHandler(.failure(.otherError(error)))
        }
    }
}


//
//  SchoolListViewModelTests.swift
//  20240211-TanmayChandraNath-NYCSchoolsTests
//
//  Created by Tanmay Chandra Nath on 11/02/24.
//

import XCTest
@testable import _0240211_TanmayChandraNath_NYCSchools

final class SchoolListViewModelTests: XCTestCase {
    
    var viewModelUnderTest: SchoolListViewModel!
    var reader: SchoolListFileReader!

    override func setUpWithError() throws {
        reader = SchoolListFileReader()
        viewModelUnderTest = SchoolListViewModel(callerDelegate: reader)
    }

    override func tearDownWithError() throws {
        viewModelUnderTest = nil
        reader = nil
    }
    
    func testIfBothApiGetsCalled() {
        viewModelUnderTest.fetchDetails()
        XCTAssertTrue(reader.didBothApiCalled)
    }
    
    // ToDo: Write other test cases to test different functionalitites of View Model
}
